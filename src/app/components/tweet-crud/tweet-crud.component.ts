import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tweet-crud',
  templateUrl: './tweet-crud.component.html',
  styleUrls: ['./tweet-crud.component.scss']
})
export class TweetCrudComponent implements OnInit {

  myTweetData: any[] = [];

  constructor() {
    // TODO J2 Exercice 5 : valoriser myTweetData avec une liste statique de données
    // Vérifier dans la console le bon fonctionnement
    this.myTweetData = [];
  }

  ngOnInit() {
  }

}