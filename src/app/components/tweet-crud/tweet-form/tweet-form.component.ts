import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tweet-form',
  templateUrl: './tweet-form.component.html',
  styleUrls: ['./tweet-form.component.scss']
})
export class TweetFormComponent implements OnInit {

  constructor() { }

  newTweetData: any = {
    isVerified: true,
    like: 0,
    comment: 0,
    retweet: 0,
    stats: 0,
    id: Math.random()
  };

  ngOnInit() {

  }

  onClickSubmit(): void {
    // TODO : Exercice 8 : transmission du nouveau tweet à la liste
  }

}
